// -*- C++ -*-
#ifndef DATA_H
#define DATA_H

#include <boost/math/constants/constants.hpp>

#include "hfv.h"

using boost::math::constants::pi;

namespace ho
{
  namespace pho
  {
    struct ExactSolution
    {

      typedef std::function<TensorType(const Point &)> DiffusivityType;
      typedef std::function<Real(const Point &)> PotentialType;
      typedef std::function<Real(const Point &)> LoadType;

      typedef std::function<Real(const Point &)> ExactSolutionType;
      typedef std::function<Real(const Point &)> InitialDataType;
      typedef std::function<Real(const Point &)> BoundaryType;
      typedef std::function<Real(const Point &)> BoundaryNeumannType;

      typedef std::function<bool(const Face &)> DefBoundaryConditionType;

      typedef std::function<Eigen::Matrix<Real, DIM, 1>(const Point &)> ExactGradientType;
      typedef std::function<Eigen::Matrix<Real, DIM, 1>(const Point &)> FieldType;


      std::string description_tex;
      Real Advection_param;
      DiffusivityType Lambda;
      PotentialType Phi;
      FieldType V;
      LoadType f;

      ExactSolutionType u;

      DefBoundaryConditionType isDirichlet;
      DefBoundaryConditionType isNeumann;



      BoundaryType u_b;
      BoundaryNeumannType g_n;

      ExactGradientType G;


    };

    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------

    struct Regular : public ExactSolution
{

  /*  Regular(const Real & _lx = 10., const Real & _ly = 0.05, const Real & _eps = 0.01,  const Real & _v = -1.)
      {


      description_tex = "according to the exact solution $u = xy + \\epsilon $ ; with $\\epsilon = " + to_string(_eps) +"$; $\\Lambda  = diag ( " + to_string(_lx)+ ";" + to_string(_ly) +")$ and potential $\\Phi =  \\frac{v}{2} (x^2 +y^2)$ with $v =" + to_string(_v) + "$." ;

      Advection_param =_v;


      isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( x == 1.) ;
        bool D_2 = ( x == 0.);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        //bool Dir = D_1 || D_2;
        bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      Lambda = [_lx,_ly](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lx , 0. ,
                0. , _ly ;
          return  L;
        };


	Phi =[_v](const Point & x) -> Real {
	return _v * 0.5* ( x(0)*x(0) + x(1) * x(1) ) ;
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
            x(0) * _v ,
            x(1) * _v;
          return  - this->Lambda(x) * res;
        };



        u = [_eps](const Point & x) -> Real {
            return x(0)*x(1) + _eps;
        };

        u_b =[this](const Point & x) -> Real {
            return this->u(x);
        };

        g_n =[](const Point & x) -> Real {

            return 0.;
        };

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [_lx, _ly, _eps, _v](const Point & x) -> Real {
            Real diff = _lx + _ly ;
            Real conv = _v * (2.*x(0)*x(1) + _eps);
            return  -diff* conv;
        };
      }

    };
*/

    /*Regular(const Real & _nu = 1. , const Real & _v = 25., const Real & _eps = 0.2 , const Real _rho =  0.2)
      {

      description_tex = "exact solution $u = \\frac{e^x}{v} \\sin(\\rho \\pi (y + \\epsilon)$ with $\\Lambda = " + to_string(_nu) +"I_2$ and potential $\\Phi = e^{\\cos(vx)} + v (0.5 y^2 + y)$ " ;

      Advection_param =  1./0.7;//_v;

      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1. ;
          return  _nu * L;
        };


	Phi =[_v](const Point & x) -> Real {
        return exp(cos(_v*x(0))) + _v * (x(1)*(1. + 0.5*x(1)));
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
        Eigen::Matrix<Real, 2, 1> res;
        res <<
            - sin(_v*x(0)) * exp(cos(_v * x(0))),
            x(1) + 1 ;
        return  - _v * this->Lambda(x) * res;
        };



    u = [_v,_eps,_rho](const Point & x) -> Real {
            return exp(x(0)) * sin(_rho * pi<Real>() * (x(1) + _eps)) / _v;
        };

    u_b =[this](const Point & x) -> Real {
        return this->u(x);
	};

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [this, _nu, _v, _eps, _rho](const Point & x) -> Real {
            Real _u = this->u(x);
            Real lapla = _v * ( 1. + _v * exp(cos(_v * x(0))) * ( pow(sin(_v* x(0)),2) - cos(_v * x(0))) );
            Real grad_1 = (x(1) +1 ) * _rho * pi<Real>() * cos (_rho * pi<Real>() * (x(1) + _eps)) ;
            Real grad_2 = sin(_v * x(0)) * sin(_rho * pi<Real>() * (x(1) + _eps)) * exp(cos(_v*x(0))) ;
            return  -_nu* ( _u * ( 1 - pow(_rho * pi<Real>(),2)+ lapla) + exp(x(0))*(grad_1 - grad_2) ) ;
        };
      }

    };*/

/*
Regular(const Real & _nu = 1.,  const Real & _v = 1.)
      {


      description_tex = "according to the exact solution $u = \\left (  x - \\exp \\left ( \\frac{2v(x-1)}{\\mu} \\right ) \\right ) \\left (  y^2 - \\exp \\left ( \\frac{3v(y-1)}{\\mu} \\right ) \\right ) +1 $ ; with $\\mu = " + to_string(_nu) +"$ and potential $\\Phi = - v ( 2 x + 3 y - 5) $ with $v =" + to_string(_v) + "$." ;

      Advection_param =_v;


      isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( x == 1.) ;
        bool D_2 = ( x == 0.);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        //bool Dir = D_1 || D_2;
        bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1. ;
          return  _nu * L;
        };


	Phi =[_v](const Point & x) -> Real {
	return -_v * (2. * x(0) + 3.*x(1) ) ;
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
            -2. * _v ,
            -3. * _v;
          return  - this->Lambda(x) * res;
        };



        u = [_nu,_v](const Point & x) -> Real {
            Real exp_x = exp(2.*_v * (x(0) - 1.)/_nu) ;
            Real exp_y = exp(3.*_v * (x(1) - 1.)/_nu)  ;
            Real alpha = x(0) - exp_x;
            Real beta  = x(1)*x(1) -exp_y;
            return alpha * beta + 1.;
        };

        u_b =[this](const Point & x) -> Real {
            return this->u(x);
        };

        g_n =[](const Point & x) -> Real {

            return 0.;
        };

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [_nu, _v](const Point & x) -> Real {
            Real exp_x = exp(2.*_v * (x(0) - 1.)/_nu) ;
            Real exp_y = exp(3.*_v * (x(1) - 1.)/_nu)  ;
            Real alpha = x(0) - exp_x;
            Real beta  = x(1)*x(1) -exp_y;
            Real Cont_1 = beta * ( -2.*_v + (1./_nu)*pow(2.*_v , 2) * exp_x * (1. -1./_nu));
            Real Cont_2 = alpha * (2. - 6.*_v *x(1) + (1./_nu) * pow(3.*_v, 2 ) * exp_y * (1. - 1./ _nu));

            return  -_nu * (Cont_1 + Cont_2);
        };
      }

    };
*/

 /*   Regular(const Real & v_x = 64. , const Real & v_y = 64., const Real & L_1 = 0., const Real L_2 = 50.34 , const Real L_3 = 0. )
      {

      description_tex = "exact solution $u = e^{x+y}$ with ambda = " ; //+ to_string(_nu) +"I_2$ and potential $\\Phi = " + to_string(_v) + " \\left ( x + \\frac{x^2}{2} \\right ) $" ;

      Advection_param = (v_x + v_y + L_1 +L_2 + L_3) / 5.;

      Lambda = [](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1. ;
          return   L;
        };


	Phi =[v_x,v_y,L_1,L_2,L_3](const Point & x) -> Real {
        Real t_0 = v_x *x(0) + v_y *x(1);
        Real t_1 = (L_1 / (2.*pi<Real>()))*sin(2.*pi<Real>()*x(0))*sin(2.*pi<Real>()*x(1));
        Real t_2 = (L_2 / (2.*pi<Real>())) * (0.25 * sin(4.*pi<Real>()*x(0)) + pi<Real>()*x(0));
        Real t_3 = L_3 * x(0)*x(1) ;

        return -1* (- (t_0 + t_1 + t_2 + t_3));
	};

	V = [this,v_x,v_y,L_1,L_2,L_3](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
        Eigen::Matrix<Real, 2, 1> res_0;
            res_0 <<
                v_x + L_3 * x(1),
                v_y + L_3 * x(0);
        Eigen::Matrix<Real, 2, 1> res_1;
            res_1 <<
                L_1 * cos(2.*pi<Real>()*x(0))*sin(2.*pi<Real>()*x(1)) + L_2 * std::pow(cos(2.*pi<Real>()*x(0)),2),
                L_1 * sin(2.*pi<Real>()*x(0))*cos(2.*pi<Real>()*x(1));

          return  -1 * this->Lambda(x) * (res_0 + res_1);
        };



        u = [](const Point & x) -> Real {
            return exp(x(0)+x(1));
        };

        u_b =[this](const Point & x) -> Real {
            return this->u(x);
        };

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [this, L_1,L_2](const Point & x) -> Real {
            Real div = - 4.* pi<Real>() * sin(2.*pi<Real>()*x(0)) * (L_1 * sin(2.*pi<Real>()*x(1)) +  L_2 * cos(2.*pi<Real>()*x(0)));
            Real _u = this->u(x);
            Real V_scal = this->V(x)(0,0) + this->V(x)(1,0);

            return  _u * (-2.  + V_scal  -1 *  div);
        };
      }

    }; */
/*

    Regular(const Real & _nu = 1.,  const Real & _v = 200.)
      {


      description_tex = "$= 1$ on $\\{x =0\\}$ and $\\{x = 1\\}$ ; Neumann $=0$ elsewhere;  with $f = 0$ ; $\\Lambda = " + to_string(_nu) +"I_2$ and potential $\\Phi = \\log \\left ( \\frac{1}{v} + x \\right )$ with $v =" + to_string(_v) + "$." ;

      Advection_param =_v;


      isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( x == 1.) ;
        bool D_2 = ( x == 0.);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1. ;
          return  _nu * L;
        };


	Phi =[_v](const Point & x) -> Real {
	return log(1./_v + x(0));
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
             _v / ( 1. + _v * x(0) ),
            0;
          return  - this->Lambda(x) * res;
        };



        u = [_v](const Point & x) -> Real {
            Real alpha = 2.*_v /(2. + _v);
            return (_v/(1. + _v *x(0) )) * (alpha * x(0) * (1/_v + 0.5 * x(0)) + 1./_v  );
        };

        u_b =[this](const Point & x) -> Real {
            return this->u(x);
        };

        g_n =[](const Point & x) -> Real {

            return 0.;
        };

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };
*/

    Regular(const Real & _nu = 1000.,  const Real & _v = 200.)
      {


      description_tex = "$= 1$ on $\\{x =0\\}$ and $\\{x = 1\\}$ ; Neumann $=0$ elsewhere;  with $f = 0$ ; $\\Lambda = " + to_string(_nu) +"I_2$ and potential $\\Phi = \\log \\left ( \\frac{1}{v} + x \\right )$ with $v =" + to_string(_v) + "$." ;

      Advection_param =_v;


      isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( x == 1.) ;
        bool D_2 = ( x == 0.);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , _nu ;
          return   L;
        };


	Phi =[_v](const Point & x) -> Real {
	return log(1./_v + x(0));
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
             _v / ( 1. + _v * x(0) ),
            0;
          return  - this->Lambda(x) * res;
        };



        u = [_v](const Point & x) -> Real {
            Real alpha = 2.*_v /(2. + _v);
            return (_v/(1. + _v *x(0) )) * (alpha * x(0) * (1/_v + 0.5 * x(0)) + 1./_v  );
        };

        u_b =[this](const Point & x) -> Real {
            return this->u(x);
        };

        g_n =[](const Point & x) -> Real {

            return 0.;
        };

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };
/*
Regular(const Real & _nu = 1., const Real & u_0 = 2.,  const Real & _v = 15.)
      {


      description_tex = "$= "+ to_string(u_0) +" $ on $\\{x =0\\}$ and $= 1$ $\\{x = 1\\}$ ; Neumann $=0$ elsewhere;  with $f = 0$ ; $\\Lambda = " + to_string(_nu) +"I_2$ and potential $\\Phi =  \\log \\left ( - \\left ( x + \\frac{1}{v} \\right )   \\left ( x - \\frac{1+v}{v}\\right )  \\right )$ with $v =" + to_string(_v) + "$." ;

      Advection_param =_v;


      isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( x == 1.) ;
        bool D_2 = ( x == 0.);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1. ;
          return  _nu * L;
        };


	Phi =[_v](const Point & x) -> Real {
	return  log( - (x(0) + 1./_v) * (x(0) - 1 - 1./_v )) ;
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
            1./(x(0) + 1./_v) +1./(x(0) - 1. - 1./_v),
            0;
          return  - this->Lambda(x) * res;
        };



        u = [u_0, _v](const Point & x) -> Real {
            Real alpha = (1. - u_0) * (1. +_v)/(_v*_v + _v + 1.) ;
            Real P = pow(x(0),3)/3. - pow(x(0),2)/2. - x(0)*(1. + _v)/(_v*_v);
            Real exp_term = -(x(0) + 1./_v ) * (x(0) -1. - 1./_v);
            return (u_0 * (1.+_v) / (_v *_v) - alpha * P )/ exp_term;
        };

        u_b =[this](const Point & x) -> Real {
            return this->u(x);
        };

        g_n =[](const Point & x) -> Real {

            return 0.;
        };

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };
    */
/*
Regular(const Real & _nu = 1., const Real & u_0 = 1.5,  const Real & _v = 10.)
      {


      description_tex = "$= "+ to_string(u_0) +" $ on $\\{x =0\\}$ and $= 1$ $\\{x = 1\\}$ ; Neumann $=0$ elsewhere;  with $f = 0$ ; $\\Lambda = " + to_string(_nu) +"I_2$ and potential $\\Phi = - \\log \\left ( - \\left ( x + \\frac{1}{v} \\right )   \\left ( x - \\frac{1+v}{v}\\right )  \\right )$ with $v =" + to_string(_v) + "$." ;

      Advection_param =_v;


      isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( x == 1.) ;
        bool D_2 = ( x == 0.);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1. ;
          return  _nu * L;
        };


	Phi =[_v](const Point & x) -> Real {
	return - log( - (x(0) + 1./_v) * (x(0) - 1 - 1./_v )) ;
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
            -1./(x(0) + 1./_v) -1./(x(0) - 1. - 1./_v),
            0;
          return  - this->Lambda(x) * res;
        };



        u = [u_0, _v](const Point & x) -> Real {
            Real alpha = _v * (2+_v) * (1. - u_0) /((1+_v)*2.*log(1+_v)) ;
            Real P = -(x(0) + 1./_v ) * (x(0) -1. - 1./_v);
            Real log_term = log(1.+_v *x(0)) - log(1 +_v -_v*x(0)) + log(1. +_v) ;
            return P * (u_0 * _v*_v /(1.+_v) + alpha * _v * log_term /(_v +2.));
        };

        u_b =[this](const Point & x) -> Real {
            return this->u(x);
        };

        g_n =[](const Point & x) -> Real {

            return 0.;
        };

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };

*/
    /*Regular(const Real & _nu = 1., const Real & u_0 = 2,  const Real & _v = 5.)
      {


      description_tex = "$= "+ to_string(u_0) +" $ on $\\{x =0\\}$ and $= 1$ $\\{x = 1\\}$ ; Neumann $=0$ elsewhere;  with $f = 0$ ; $\\Lambda = " + to_string(_nu) +"I_2$ and potential $\\Phi = v x $ with $v =" + to_string(_v) + "$." ;

      Advection_param =_v;


      isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( x == 1.) ;
        bool D_2 = ( x == 0.);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1. ;
          return  _nu * L;
        };


	Phi =[_v](const Point & x) -> Real {
	return _v * x(0);
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
            _v,
            0;
          return  - this->Lambda(x) * res;
        };



        u = [u_0, _v](const Point & x) -> Real {
            Real alpha = _v * (u_0 - exp(_v)) / (exp(_v)-1.);
            return exp(-_v*x(0)) * ( u_0 - (alpha / _v) * ( exp(_v*x(0)) -1.) );
        };

        u_b =[this](const Point & x) -> Real {
            return this->u(x);
        };

        g_n =[](const Point & x) -> Real {

            return 0.;
        };

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };*/


/*

     Regular(const Real & _nu = 1.,  const Real & _v = 200.)
      {


      description_tex = "$=1$ on $\\{x =0\\}$ and $\\{x = 1\\}$ ; Neumann $=0$ elsewhere;  with $\\Lambda = " + to_string(_nu) +"I_2$ and potential $\\Phi = -\\log \\left ( \\frac{1}{v} + x \\right) $ with $v =" + to_string(_v) + "$." ;
      //description_tex = "$= e^{-\\Phi}$ on $\\{x =0\\}$ and $\\{x = 1\\}$ ; Neumann $=0$ elsewhere;  with $\\Lambda = " + to_string(_nu) +"I_2$ and potential $\\Phi = -\\log \\left ( \\frac{1}{v} + x \\right) $ with $v =" + to_string(_v) + "$." ;

      Advection_param =_v;


      isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( x == 1.) ;
        bool D_2 = ( x == 0.);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1. ;
          return  _nu * L;
        };


	Phi =[_v](const Point & x) -> Real {
	return -log(1./_v + x(0));
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
            - _v / ( 1. + _v * x(0) ),
            0;
          return  - this->Lambda(x) * res;
        };



        u = [this,_v](const Point & x) -> Real {
            Real alpha = -  pow(_v, 2) / (( 1 +_v ) * log( 1 + _v));
            return (1./_v +x(0)) * (_v + alpha * log(1. + _v * x(0)));
        };

        u_b =[this](const Point & x) -> Real {
            return this->u(x);
        };

        g_n =[](const Point & x) -> Real {

            return 0.;
        };

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }

    };

*/

/*
    Regular(const Real & _nu = 1. , const Real & _v = 5., const Real & _eps = std::pow(10., 0))
      {

      description_tex = "exact solution is $u = x y + \\epsilon$ with $\\Lambda = " + to_string(_nu) +"I_2$ and potential $\\Phi = v  \\left ( x + \\frac{x^2}{2} \\right )$; $v =" +to_string(_v) +"$ and $\\epsilon =" +to_string(_eps) +"$."  ;

      Advection_param =_v;


      isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = (0. < x &&  x < 0.5) && (y == 0.);
        bool D_2 = (y==1.) && (0. < x && x < 1.);
        //bool Dir = D_1 || D_2;
        bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1. ;
          return  _nu * L;
        };


	Phi =[_v](const Point & x) -> Real {
	return _v* (x(0) + 0.5*x(0)*x(0));
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
            _v *(1+ x(0)),
            0;
          return  - this->Lambda(x) * res;
        };



        u = [_eps](const Point & x) -> Real {
          return x(0)*x(1) + _eps;
        };

        u_b =[this](const Point & x) -> Real {
            return this->u(x);
        };

        g_n =[](const Point & x) -> Real {

            return 5.;
        };

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [_nu,_v, _eps](const Point & x) -> Real {
          return  -_nu*_v*(x(1)*(1. + 2.*x(0)) + _eps);
        };
      }

    };
    */

   /* Regular(const Real & _nu = 1. , const Real & _v = 10000. )
      {
      description_tex = "exact solution $u = x + y$ with $\\Lambda = " + to_string(_nu) +"I_2$ and potential $\\Phi = " + to_string(_v) + "\\frac{1}{2} \\left ( x^2 + y^2   \\right ) $" ;

      Advection_param =_v;
      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1. ;
          return  _nu * L;
        };


	Phi =[_v](const Point & x) -> Real {
	return 0.5*_v* (x(0)*x(0) + x(1)*x(1));
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
            _v * x(0),
            _v * x(1);
          return  - this->Lambda(x) * res;
        };



        u = [this](const Point & x) -> Real {
          return x(0)+x(1);
        };

	u_b =[this](const Point & x) -> Real {
	return this->u(x);
	};

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [_nu,_v](const Point & x) -> Real {
          return  -3.*_nu*_v*(x(0) + x(1));
        };
      }

    };*/

/*Regular(const Real & _nu = 0.001 , const Real & _v = -1000.*1000. )
      {
      description_tex = "exact solution $u = x  y$ with $\\Lambda = " + to_string(_nu) +"I_2$ and potential $\\Phi = " + to_string(_v) + "\\frac{1}{2} \\left ( x^2 + y^2   \\right ) $" ;

      Advection_param =_v;
      Lambda = [_nu](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1. ;
          return  _nu * L;
        };


	Phi =[_v](const Point & x) -> Real {
	return 0.5*_v* (x(0)*x(0) + x(1)*x(1));
	};

	V = [this,_v](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
	 Eigen::Matrix<Real, 2, 1> res;
          res <<
            _v * x(0),
            _v * x(1);
          return  - this->Lambda(x) * res;
        };



        u = [this](const Point & x) -> Real {
          return x(0)*x(1);
        };

	u_b =[this](const Point & x) -> Real {
	return this->u(x);
	};

        G = [](const Point & x) -> Eigen::Matrix<Real, 2, 1> {
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          pi<Real>() * cos(pi<Real>()*x(0))*sin(pi<Real>()*x(1)),
          pi<Real>() * sin(pi<Real>()*x(0))*cos(pi<Real>()*x(1));
          return res;
        };

        f = [_nu,_v](const Point & x) -> Real {
          return  -4.*_nu*_v*(x(0) *x(1));
        };
      }

    };*/





  } // namespace pho
} // namespace ho

#endif
