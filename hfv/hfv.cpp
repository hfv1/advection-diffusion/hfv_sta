#include "hfv.h"


#include <Common/GetPot>

#include <boost/integer/static_min_max.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/numeric/ublas/vector.hpp>

#include "Common/chrono.hpp"

#include "data.h"
#include "postProcessing.h"

#include "Eigen/SparseCore"
#include "Eigen/SparseLU"
#include "Spectra/GenEigsSolver.h"
#include "Spectra/MatOp/SparseGenMatProd.h"
#include "Spectra/SymEigsSolver.h"
#include "Spectra/MatOp/SparseSymMatProd.h"
#include "Spectra/MatOp/SparseSymShiftSolve.h"



#include <unsupported/Eigen/SparseExtra>

#include <vector>

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <string.h>
#include<iomanip>

#include<fstream>
#include <sys/stat.h>
#include <sys/types.h>

//#include <boost/filesystem.hpp>

#ifndef Eta
#define Eta 1.5
#endif


#ifndef s_pos
#define s_pos std::pow(10., -11)
#endif

#ifndef s_iter
#define s_iter 50
#endif

#ifndef s_G
#define s_G 5.*std::pow(10.,-9) //-13)
#endif

#ifndef s_R
#define s_R 5.*std::pow(10., -9)
#endif

#ifndef s_R_rel
#define s_R_rel 1.*std::pow(10., -11)
#endif




using boost::math::constants::pi;

using namespace ho;
using namespace Spectra;

//------------------------------------------------------------------------------

typedef Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> IndexVectorType;
typedef Eigen::SparseMatrix<Real> SparseMatrixType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> SolutionVectorType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> ComputeFluxType;
typedef Eigen::Triplet<Real> TripletType;
typedef Eigen::Matrix<Real, 2, 2 > TensorType;
typedef std::function<TensorType(const Point &)> DiffusivityType;
typedef std::function<Eigen::Matrix<Real, 2, 1>(const Point &)> FieldType;
typedef std::function<Real(const Point &)> FctContType;


vector<Real> omegaschema_mesh(const Real & epsilon, const string & Mesh_file ,const bool Visu, const bool Spec_info ,Integer argc, char * argv[]);
vector<Real> omegaschema_mesh_harmo(const Real & epsilon, const int type_moy, const string & Mesh_file ,const bool Visu, const bool Spec_info ,Integer argc, char * argv[]);
//type_moy = 1 : harmonique par pyramide

vector<Real> nonlinearsta_mesh(const string & Mesh_file , const bool Visu , Integer argc, char * argv[]);

int bench(const string geo_mesh, const int i_max , Integer argc, char * argv[]);




main (Integer argc, char * argv[]){

    bool Full_bench;
    cout << "Full benchmark (all meshes) ? (1 for yes, 0 for no) " << endl;
    cin >> Full_bench;

    if(Full_bench){
    vector<string> Meshes;
    Meshes.resize(9);
    vector<int> Number;
    Number.resize(9);
    Meshes[0]="hexagonal";
    Number[0]=5;
    Meshes[1]="mesh1";
    Number[1]=7;
    Meshes[2]="mesh2",
    Number[2]=7;
    Meshes[3]="mesh3";
    Number[3]=5;
    Meshes[4]="mesh4_1";
    Number[4]=6;
    Meshes[5]="mesh4_2";
    Number[5]=6;
    Meshes[6]="mesh5";
    Number[6]=6;
    Meshes[7]="mesh_quad";
    Number[7]=7;
    Meshes[8]="hexat";
    Number[8]=5;

    for(int i=0; i<9; i++){
        bench(Meshes[i],Number[i] , argc, argv);

    }



    }

    else{
    string  geo_mesh ; // = "hexagonal";
    int  i_max ; //= 5;
    cout << "Mesh geometry ? (name without _, ex : hexagonal or mesh3)" << endl;
    cin >> geo_mesh;
    cout << "Number of meshes ? " << endl;
    cin >> i_max;
    bench(geo_mesh,i_max , argc, argv);
    }



}
//------------------------------------------------------------------------------
int bench(const string geo_mesh, const int i_max , Integer argc, char * argv[]){
    bool Visu = false;

    bool Spectral_info = false;



    const char *dir = ("mkdir " + geo_mesh).c_str();
    system(dir);



    //cout << "Spectral info ? (1 for yes, 0 for no) " << endl;
    //cin >> Spectral_info;
    //std::string Mesh_base = "../meshes/";
    //std::string Mesh_file = Mesh_base + geo_mesh +"_" + std::to_string(i_max) +".typ1";
    //omegaschema_mesh(0., Mesh_file, Visu ,argc, argv);

    ho::pho::Regular sol;
    Real v = sol.Advection_param;
    string desc = sol.description_tex;

    const int nb_schema_lin = 3;

    const int nb_schema = nb_schema_lin +1;

    std::string Mesh_base = "../meshes/";

    vector<vector<vector<Real>>> Data_errors;
    Data_errors.resize(nb_schema);
    for(int i =0 ; i < nb_schema ; i ++){
        Data_errors[i].resize(i_max);
    }

    //omegaschema(0., T, N_iter, Visu ,argc, argv);
    //omegaschema(1./v ,argc, argv);
    //omegaschema(0.5, T, N_iter, Visu  ,argc, argv);
    //omegaschema(1., T, N_iter, Visu ,argc, argv);
    for(int i=1; i< i_max+1 ; i++){
        std::string Mesh_file = Mesh_base + geo_mesh +"_" + std::to_string(i) +".typ1";

        Data_errors[0][i-1] = nonlinearsta_mesh(Mesh_file, Visu,argc, argv);
        Data_errors[1][i-1] = omegaschema_mesh(0., Mesh_file, Visu, Spectral_info,argc, argv);
        Data_errors[2][i-1] = omegaschema_mesh(1., Mesh_file, Visu , Spectral_info,argc, argv);
        Data_errors[3][i-1] = omegaschema_mesh_harmo(1., 1 ,Mesh_file, Visu , Spectral_info,argc, argv);
    } //for i

    //create file for errors

    std::ofstream myfile;
    myfile.open (geo_mesh + "/errors.csv");
    myfile << "Première cellule premiere colonne .\n";
    myfile <<"Description "<<"," <<"Convection diffusion, omega schéma .\n";
    myfile <<"Eta"<<"," << Eta  << endl;
    myfile <<"Maillage"<<"," << geo_mesh  << endl;
    myfile <<"" << endl;
    for(int sch = 0; sch< nb_schema; sch ++){
        myfile << "Meshsize, L^2 error (rel), H^1  error (rel), Minimum vol, Minimum edge, Condition Number,\n";
        for(int mesh_int = 0; mesh_int < i_max; mesh_int ++){
            myfile << Data_errors[sch][mesh_int][0] <<","<< Data_errors[sch][mesh_int][1] <<","<< Data_errors[sch][mesh_int][2] <<","<< Data_errors[sch][mesh_int][3] <<","<< Data_errors[sch][mesh_int][4]<< ","<< Data_errors[sch][mesh_int][5]<<endl;
            }//mmesh_int
    }//for sch
      myfile.close();

      //create file graph

    std::ofstream file_graph;
    file_graph.open (geo_mesh + "/errors_graph.txt");
    file_graph << nb_schema << "," << i_max<< endl;
    file_graph << geo_mesh  << endl;
    file_graph << desc << "," << v << endl;
    file_graph << Eta  << endl;
    file_graph << "non-linear" << "," ;
    file_graph << "HMM" << "," ;
    file_graph << "Exp-fitt" << "," ;
    file_graph << "Exp-fitt-harmo" << "," ;
    file_graph << endl ;
    for(int sch = 0; sch< nb_schema; sch ++){
        for(int mesh_int = 0; mesh_int < i_max; mesh_int ++){
            file_graph << Data_errors[sch][mesh_int][0] <<","<< Data_errors[sch][mesh_int][1] <<","<< Data_errors[sch][mesh_int][2]
            << "," << Data_errors[sch][mesh_int][3] <<","<< Data_errors[sch][mesh_int][4] <<","<< Data_errors[sch][mesh_int][5] << endl;
            }//mmesh_int
    }//for sch
      file_graph.close();


    vector<std::string> Names;
    Names.resize(4);
    Names[0] = "nonlin";
    Names[1] = "HMM";
    Names[2] = "expfittari";
    Names[3] = "expfittharmo";

    for(int sch = 0; sch< nb_schema; sch ++){
        std::ofstream file_tikz;
        std::string path = geo_mesh + "/" +Names[sch];

        // mkdir(dir);
        file_tikz.open (path);
        file_tikz << "meshsize errl2 errh1 min_cell min_edge" << endl ;
        for(int mesh_int = 0; mesh_int < i_max; mesh_int ++){
            file_tikz << Data_errors[sch][mesh_int][0] <<" "<< Data_errors[sch][mesh_int][1] <<" "<< Data_errors[sch][mesh_int][2]
            << " " << Data_errors[sch][mesh_int][3] <<" "<< Data_errors[sch][mesh_int][4] << endl;
            }//mmesh_int
        file_tikz.close();
    }//for sch


    std::cout << "Done" << std::endl;
    return 0;
}

//linear (omega)

vector<Real> omegaschema_mesh(const Real & epsilon, const string & Mesh_file ,const bool Visu ,const bool Spec_info ,Integer argc, char * argv[]){

  std::string sep = "\n----------------------------------------\n";
  Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
  std::cout << "test1 ? "  << std::endl;

  // Read options and data
  GetPot options(argc, argv);

  std::string mesh_file = Mesh_file;

  // Pretty-print floating point numbers
  std::cout.precision(2);
  std::cout.flags(std::ios_base::scientific);

  std::cout << "test2 ? "  << std::endl;
  // Create mesh
  std::shared_ptr<Mesh> Th(new Mesh());
  if(mesh_file.find("typ1")!=std::string::npos)
    Th->readFVCA5File(mesh_file.c_str());
  else if(mesh_file.find("dgm")!=std::string::npos)
    Th->readDGMFile(mesh_file.c_str());
  else {
    std::cerr << "Unknown mesh format" << std::endl;
    exit(1);
  }

  std::cout << "test3 ? "  << std::endl;

  Th->buildFaceGroups();
  std::cout << "MESH: " << mesh_file << std::endl;
  std::cout << FORMAT(50) << "num_cells" << Th->numberOfCells() << std::endl;
  std::cout << FORMAT(50) << "num_faces" << Th->numberOfFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws_cells" << Th->numberOfCells()  << std::endl;
  std::cout << FORMAT(50) << "num_unknws_faces" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws" << Th->numberOfCells() +  Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws_with_elimination" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;

  //------------------------------------------------------------------------------
  // Estimate mesh size
  std::cout<< FORMAT(50) << "Paramètre de stabilisation" << Eta << std::endl;

  Real h = Th->meshsize();
  std::cout << FORMAT(50) << "meshsize" << h << std::endl;


  //------------------------------------------------------------------------------
  // Exact solution

  ho::pho::Regular sol;

  // omega function and modified tensor and field

  //Real epsilon = 0.001;

  const FctContType omega = [epsilon,sol](const Point & x) -> Real {
    Real arg = - epsilon * sol.Phi(x);
    return std::exp(arg);
  };

  const DiffusivityType Lambda_omega = [sol, omega](const Point & x) -> TensorType {
    return omega(x) * sol.Lambda(x);
  };

  const FieldType V_omega = [sol, omega, epsilon](const Point & x) -> Eigen::Matrix<Real, 2, 1>  {
    return (1. - epsilon) * omega(x) * sol.V(x);
  };



  //------------------------------------------------------------------------------
  // Assemble matrix
  //------------------------------------------------------------------------------


  int nunkw = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
  int n_vol_unkw =  Th->numberOfCells();

  // Pre computation
  std::cout << "Pre-computation begins" << endl;
  common::chrono c_precomput;
  c_precomput.start();

  std::vector<vector<vector<Real>>> A;
  comp_A_elt(Th.get(), Lambda_omega, Eta, A);

  std::vector<vector<Real>> B;
  comp_B_elt(Th.get(), A, B );

  std::vector<Real> Alpha;
  comp_Alpha_elt(Th.get(), B, Alpha );

  std::vector<Real>  Diff_Int;
  comp_DiffInt(Th.get(),  Lambda_omega, Diff_Int );


  std::vector<vector<Real>> Conv_N;
  std::vector<vector<Real>> Conv_P;
  comp_Conv_elt(Th.get(), Diff_Int, V_omega,  Conv_N, Conv_P);



  std::vector<Real>  Conv_N_sum;
  std::vector<Real>  Conv_P_sum;
  comp_Alpha_elt(Th.get(), Conv_N , Conv_N_sum);
  comp_Alpha_elt(Th.get(), Conv_P , Conv_P_sum);

  //discrétisation donnée au bord (relevement)
  SolutionVectorType X_b = SolutionVectorType::Zero(nunkw);
  SolutionVectorType H_b = SolutionVectorType::Zero(nunkw);
  for(int iT = 0; iT< Th->numberOfCells(); iT ++){
    const Cell & T = Th->cell(iT);
    const Integer m = T.numberOfFaces();
    for(int iF_loc = 0; iF_loc < m; iF_loc ++){
        const int iF = T.faceId(iF_loc);
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
       	if(F.isBoundary()){
            X_b(iF) =  sol.u_b(x_F);
            H_b(iF) = sol.u_b(x_F)/omega(x_F);
        }//if
      }// for iF_loc
  }// for iT

  c_precomput.stop();
  cout << "Pre-computation ends" << endl;
  std::cout << FORMAT(50) << "time_precomputation"  << c_precomput.diff() << std::endl;

    common::chrono c_assembly;
    c_assembly.start();
    cout << "matrix assembly begins" << endl;



    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_edge_max =Th->maximumNumberOfFaces();

    int n_edge_Dir = 0.;
    for(int iF =0; iF < n_edge; iF ++){
        if(sol.isDirichlet(Th->face(iF))){n_edge_Dir += 1;}
    }

    //compute h -> u matrixes
    std::vector<TripletType> Triplets_invD_vol;
    Triplets_invD_vol.reserve(n_vol);
    std::vector<TripletType> Triplets_D_vol;
    Triplets_D_vol.reserve(n_vol);
    std::vector<TripletType> Triplets_D_edge;
    Triplets_D_edge.reserve(n_edge);

    for(int iT = 0; iT < n_vol ; iT ++){
        const Cell & T = Th->cell(iT);
        const Point & x_T = T.center();
        const Real omega_T = omega(x_T);
        Triplets_D_vol.push_back(TripletType(iT, iT, 1./omega_T));
        Triplets_invD_vol.push_back(TripletType(iT, iT, omega_T));
    } //for iT

    SparseMatrixType invD_vol(n_vol, n_vol);
    invD_vol.setFromTriplets(Triplets_invD_vol.begin(), Triplets_invD_vol.end());
    SparseMatrixType D_vol(n_vol, n_vol);
    D_vol.setFromTriplets(Triplets_D_vol.begin(), Triplets_D_vol.end());

    for(int iF = 0; iF < n_edge ; iF ++){
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
        const Real omega_F = omega(x_F);
        Triplets_D_edge.push_back(TripletType(iF, iF, 1./omega_F));
    } //for iF

    SparseMatrixType D_edge(n_edge, n_edge);
    D_edge.setFromTriplets(Triplets_D_edge.begin(), Triplets_D_edge.end());



    // compute triplets ( h linear system)
    std::vector<TripletType> Triplets_invM1;
    Triplets_invM1.reserve(n_vol);
    std::vector<TripletType> Triplets_M2;
    Triplets_M2.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_M3;
    Triplets_M3.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_M4;
    Triplets_M4.reserve(n_vol* n_edge_max * n_edge_max);

    triplets_invM1andM2(Th.get(),  Alpha, B, Conv_N_sum,  Conv_P,
    Triplets_invM1,  Triplets_M2);

    triplets_M3andM4(Th.get(),  B, A, Conv_N, Conv_P,
    Triplets_M3, Triplets_M4);

    //compute matrix
    SparseMatrixType invM1(n_vol, n_vol);
    invM1.setFromTriplets(Triplets_invM1.begin(), Triplets_invM1.end());
    invM1 = invD_vol * invM1;
    SparseMatrixType M2(n_vol, n_edge);
    M2.setFromTriplets(Triplets_M2.begin(), Triplets_M2.end());
    M2 = M2 * D_edge;
    SparseMatrixType M3(n_edge, n_vol);
    M3.setFromTriplets(Triplets_M3.begin(), Triplets_M3.end());
    M3 = M3 * D_vol;
    SparseMatrixType M4(n_edge, n_edge);
    M4.setFromTriplets(Triplets_M4.begin(), Triplets_M4.end());
    M4 =M4 *D_edge;

    SolutionVectorType  Sec_m_vol = SolutionVectorType::Zero(n_vol);
    SolutionVectorType  Sec_m_edge =  SolutionVectorType::Zero(n_edge);

    comp_sec_menb( Th.get(),  H_b ,  sol.f, sol.isNeumann,  sol.g_n, A ,  B , Conv_P ,
    Sec_m_vol ,  Sec_m_edge );

    //resize matrix for Dirichlet Boundary conditions
    SparseMatrixType Resize_Dir(nunkw-n_edge_Dir,nunkw);
    int counter_Fb_Dir = 0;
    for(Integer iF = 0; iF < Th->numberOfFaces(); iF++){
        if(!(sol.isDirichlet(Th->face(iF)))){
            Resize_Dir.insert(iF - counter_Fb_Dir, iF)=1.;
        }//if isBoundary
    else{counter_Fb_Dir++;};
    }//for iF

    SparseMatrixType GLOBAL_LHS = Resize_Dir * (M4 -M3*invM1 * M2) * (Resize_Dir.transpose());
    Eigen::VectorXd GLOBAL_RHS = Resize_Dir * (Sec_m_edge - M3*invM1 * Sec_m_vol);


    c_assembly.stop();
    cout << "Matrix assembly ends" << endl;
    std::cout << FORMAT(50) << "time_assembly"  << c_assembly.diff() << std::endl;

    // Solve linear system and reconstruct mesh unknowns

    common::chrono c_solve;
    c_solve.start();

    cout << "Linear solving begins" << endl;

    SolutionVectorType X = SolutionVectorType::Zero(nunkw);
    SolutionVectorType U = SolutionVectorType::Zero(n_vol_unkw);

    Eigen::SparseLU<SparseMatrixType> solver_infty;
    // Compute the ordering permutation vector from the structural pattern of ...
    solver_infty.analyzePattern(GLOBAL_LHS);
    std::cout << FORMAT(50) << "analyse pattern OK"  << std::endl;
    // Compute the numerical factorization
    solver_infty.factorize(GLOBAL_LHS);
    std::cout << FORMAT(50) << "factorisation etat stationnaire  OK"  << std::endl;
    //Use the factors to solve the linear system
    X = (Resize_Dir.transpose())*(solver_infty.solve(GLOBAL_RHS));
    U = invM1 * (Sec_m_vol - M2*X);
    X = X +X_b;

    c_solve.stop();
    std::cout << FORMAT(50) << "fin factorisation et calcul des inconnues "  << std::endl;
    std::cout << FORMAT(50) << "time_solve"  << c_solve.diff() << std::endl;
    Real Condi;

    if(Spec_info){
        common::chrono c_spectral;
        c_spectral.start();
        std::cout << FORMAT(50) << "Computation of spectral info begins"  << std::endl;

        //SparseGenMatProd<Real> op(GLOBAL_LHS);

        //GenEigsSolver< Real, LARGEST_MAGN, SparseGenMatProd<Real> > eigs(&op, 10, 30);

        SparseMatrixType Sing_val_mat = ( GLOBAL_LHS.transpose() )* GLOBAL_LHS;
        SparseSymMatProd<Real> op(Sing_val_mat);
        //SparseSymShiftSolve<Real> op(Sing_val_mat);
        //SparseSymMatProd <Real> op( ( GLOBAL_LHS.transpose() )* GLOBAL_LHS ) ;
        SymEigsSolver< Real, BOTH_ENDS, SparseSymMatProd <Real> > eigs(&op, 2, n_edge - n_edge_Dir -10);

        // Initialize and compute
        eigs.init();
        int nconv = eigs.compute();

        // Retrieve results
        Eigen::VectorXcd evalues;
        if(eigs.info() == SUCCESSFUL)
            evalues = eigs.eigenvalues();

            Condi = sqrt(abs(evalues(0)/evalues(1)));

        std::cout << "Eigenvalues found:\n" << evalues << std::endl;


        c_spectral.stop();
        std::cout << FORMAT(50) << "Computation of spectral info ends"  << std::endl;
        std::cout << FORMAT(50) << "time_spectral_quantities"  << c_spectral.diff() << std::endl;
    }
    else{ Condi = 0. ; }


    std::cout << sep << std::endl;
    std::cout << FORMAT(50) << "Errors"  << std::endl;
    std::cout << sep << std::endl;
    //------------------------------
    // Compute errors


    Real err_uh = 0.;
    Real err_en = 0.;
    Real norm_u_l2 = 0.;
    Real norm_u_h1 = 0.;


    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        const Point & x_T = T.center();
        const int & m = T.numberOfFaces();

        //Compute L2-norm of the exact solution
        norm_u_l2 += m_T*std::pow( sol.u(x_T), 2);
        // Compute local errors in L2-norm
        err_uh += m_T*std::pow( U(iT) - sol.u(x_T), 2);

        // Compute local error in energy-like norm (discrete H^1-norm)
          for(int iF_loc=0;iF_loc<m;iF_loc++ ){
          const int & iF=T.faceId(iF_loc);
          const Face & F = Th->face(iF);
          const Real & m_F = F.measure();
          const Point&  x_F = F.barycenter();
          const Point & n_F= F.normal(x_T);
          const Real & d_F = inner_prod(n_F, x_F-x_T);

          err_en += (m_F/d_F)*std::pow(U(iT)-X(iF)-sol.u(x_T)+sol.u(x_F),2);
          norm_u_h1 += (m_F/d_F)*std::pow(sol.u(x_T)-sol.u(x_F),2);

        }// for iF_loc
      } // for iT

    norm_u_l2 = std::sqrt(norm_u_l2);
    norm_u_h1 = std::sqrt(norm_u_h1);
    err_uh = std::sqrt(err_uh);
    err_en = std::sqrt(err_en);
    std::cout << FORMAT(50) << "les erreurs affichées sont des erreurs relatives"  << std::endl;

    std::cout << FORMAT(50) << "err_uh_rel - L2 " << err_uh / norm_u_l2 << std::endl;
    std::cout << FORMAT(50) << "err_uh_rel - H1" << err_en / norm_u_h1 << std::endl;
    std::cout << FORMAT(50) << "Minimum on mesh" << U.minCoeff() << std::endl;
    std::cout << FORMAT(50) << "Minimum on edge" << X.minCoeff() << std::endl;
    std::cout << FORMAT(50) << "Condition number" << Condi << std::endl;


    // export file vtu
    if(Visu){
        std::vector<Eigen::VectorXd> coeffs_u(Th->numberOfCells());
        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
        Eigen::VectorXd uh_F (1);
        uh_F(0) =U(iT);
        coeffs_u[iT] = uh_F;
        }//for iT
        string name_file = "output/uh_lin_eps="+to_string(epsilon);
        name_file += Mesh_file.substr(10);
        name_file += ".vtu";
        char* char_name;
        char_name = &name_file[0];

        postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>(char_name, Th.get(), coeffs_u);
    } //if Visuvector<Real> Vec_data;

    vector<Real> Vec_data;
    Vec_data.push_back(h);
    Vec_data.push_back(err_uh / norm_u_l2 );
    Vec_data.push_back(err_en / norm_u_h1 );
    Vec_data.push_back(U.minCoeff());
    Vec_data.push_back(X.minCoeff());
    Vec_data.push_back(Condi);

    std::cout << sep << std::endl;
    std::cout << FORMAT(50) << "DONE"  << std::endl;
    std::cout << sep << std::endl;

    return Vec_data;
}

vector<Real> omegaschema_mesh_harmo(const Real & epsilon, const int type_moy, const string & Mesh_file ,const bool Visu, const bool Spec_info ,Integer argc, char * argv[]){

  std::string sep = "\n----------------------------------------\n";
  Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
  std::cout << "test1 ? "  << std::endl;

  // Read options and data
  GetPot options(argc, argv);

  std::string mesh_file = Mesh_file;

  // Pretty-print floating point numbers
  std::cout.precision(2);
  std::cout.flags(std::ios_base::scientific);

  std::cout << "test2 ? "  << std::endl;
  // Create mesh
  std::shared_ptr<Mesh> Th(new Mesh());
  if(mesh_file.find("typ1")!=std::string::npos)
    Th->readFVCA5File(mesh_file.c_str());
  else if(mesh_file.find("dgm")!=std::string::npos)
    Th->readDGMFile(mesh_file.c_str());
  else {
    std::cerr << "Unknown mesh format" << std::endl;
    exit(1);
  }

  std::cout << "test3 ? "  << std::endl;

  Th->buildFaceGroups();
  std::cout << "MESH: " << mesh_file << std::endl;
  std::cout << FORMAT(50) << "num_cells" << Th->numberOfCells() << std::endl;
  std::cout << FORMAT(50) << "num_faces" << Th->numberOfFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws_cells" << Th->numberOfCells()  << std::endl;
  std::cout << FORMAT(50) << "num_unknws_faces" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws" << Th->numberOfCells() +  Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws_with_elimination" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;

  //------------------------------------------------------------------------------
  // Estimate mesh size
  std::cout<< FORMAT(50) << "Paramètre de stabilisation" << Eta << std::endl;

  Real h = Th->meshsize();
  std::cout << FORMAT(50) << "meshsize" << h << std::endl;


  //------------------------------------------------------------------------------
  // Exact solution

  ho::pho::Regular sol;

  // omega function and modified tensor and field

  //Real epsilon = 0.001;

  const FctContType omega = [epsilon,sol](const Point & x) -> Real {
    Real arg = - epsilon * sol.Phi(x);
    return std::exp(arg);
  };

  const DiffusivityType Lambda_omega = [sol, omega](const Point & x) -> TensorType {
    return omega(x) * sol.Lambda(x);
  };

  const FieldType V_omega = [sol, omega, epsilon](const Point & x) -> Eigen::Matrix<Real, 2, 1>  {
    return (1. - epsilon) * omega(x) * sol.V(x);
  };



  //------------------------------------------------------------------------------
  // Assemble matrix
  //------------------------------------------------------------------------------


  int nunkw = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
  int n_vol_unkw =  Th->numberOfCells();

  // Pre computation
  std::cout << "Pre-computation begins" << endl;
  common::chrono c_precomput;
  c_precomput.start();

  std::vector<vector<vector<Real>>> A;
  comp_A_elt_harmo(Th.get(), sol.Lambda, omega, type_moy, Eta, A);

  std::vector<vector<Real>> B;
  comp_B_elt(Th.get(), A, B );

  std::vector<Real> Alpha;
  comp_Alpha_elt(Th.get(), B, Alpha );

  std::vector<Real>  Diff_Int;
  comp_DiffInt(Th.get(),  Lambda_omega, Diff_Int );


  std::vector<vector<Real>> Conv_N;
  std::vector<vector<Real>> Conv_P;
  comp_Conv_elt(Th.get(), Diff_Int, V_omega,  Conv_N, Conv_P);



  std::vector<Real>  Conv_N_sum;
  std::vector<Real>  Conv_P_sum;
  comp_Alpha_elt(Th.get(), Conv_N , Conv_N_sum);
  comp_Alpha_elt(Th.get(), Conv_P , Conv_P_sum);

  //discrétisation donnée au bord (relevement)
  SolutionVectorType X_b = SolutionVectorType::Zero(nunkw);
  SolutionVectorType H_b = SolutionVectorType::Zero(nunkw);
  for(int iT = 0; iT< Th->numberOfCells(); iT ++){
    const Cell & T = Th->cell(iT);
    const Integer m = T.numberOfFaces();
    for(int iF_loc = 0; iF_loc < m; iF_loc ++){
        const int iF = T.faceId(iF_loc);
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
       	if(F.isBoundary()){
            X_b(iF) =  sol.u_b(x_F);
            H_b(iF) = sol.u_b(x_F)/omega(x_F);
        }//if
      }// for iF_loc
  }// for iT

  c_precomput.stop();
  cout << "Pre-computation ends" << endl;
  std::cout << FORMAT(50) << "time_precomputation"  << c_precomput.diff() << std::endl;

    common::chrono c_assembly;
    c_assembly.start();
    cout << "matrix assembly begins" << endl;



    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_edge_max =Th->maximumNumberOfFaces();

    int n_edge_Dir = 0.;
    for(int iF =0; iF < n_edge; iF ++){
        if(sol.isDirichlet(Th->face(iF))){n_edge_Dir += 1;}
    }

    //compute h -> u matrixes
    std::vector<TripletType> Triplets_invD_vol;
    Triplets_invD_vol.reserve(n_vol);
    std::vector<TripletType> Triplets_D_vol;
    Triplets_D_vol.reserve(n_vol);
    std::vector<TripletType> Triplets_D_edge;
    Triplets_D_edge.reserve(n_edge);

    for(int iT = 0; iT < n_vol ; iT ++){
        const Cell & T = Th->cell(iT);
        const Point & x_T = T.center();
        const Real omega_T = omega(x_T);
        Triplets_D_vol.push_back(TripletType(iT, iT, 1./omega_T));
        Triplets_invD_vol.push_back(TripletType(iT, iT, omega_T));
    } //for iT

    SparseMatrixType invD_vol(n_vol, n_vol);
    invD_vol.setFromTriplets(Triplets_invD_vol.begin(), Triplets_invD_vol.end());
    SparseMatrixType D_vol(n_vol, n_vol);
    D_vol.setFromTriplets(Triplets_D_vol.begin(), Triplets_D_vol.end());

    for(int iF = 0; iF < n_edge ; iF ++){
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
        const Real omega_F = omega(x_F);
        Triplets_D_edge.push_back(TripletType(iF, iF, 1./omega_F));
    } //for iF

    SparseMatrixType D_edge(n_edge, n_edge);
    D_edge.setFromTriplets(Triplets_D_edge.begin(), Triplets_D_edge.end());



    // compute triplets ( h linear system)
    std::vector<TripletType> Triplets_invM1;
    Triplets_invM1.reserve(n_vol);
    std::vector<TripletType> Triplets_M2;
    Triplets_M2.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_M3;
    Triplets_M3.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_M4;
    Triplets_M4.reserve(n_vol* n_edge_max * n_edge_max);

    triplets_invM1andM2(Th.get(),  Alpha, B, Conv_N_sum,  Conv_P,
    Triplets_invM1,  Triplets_M2);

    triplets_M3andM4(Th.get(),  B, A, Conv_N, Conv_P,
    Triplets_M3, Triplets_M4);

    //compute matrix
    SparseMatrixType invM1(n_vol, n_vol);
    invM1.setFromTriplets(Triplets_invM1.begin(), Triplets_invM1.end());
    invM1 = invD_vol * invM1;
    SparseMatrixType M2(n_vol, n_edge);
    M2.setFromTriplets(Triplets_M2.begin(), Triplets_M2.end());
    M2 = M2 * D_edge;
    SparseMatrixType M3(n_edge, n_vol);
    M3.setFromTriplets(Triplets_M3.begin(), Triplets_M3.end());
    M3 = M3 * D_vol;
    SparseMatrixType M4(n_edge, n_edge);
    M4.setFromTriplets(Triplets_M4.begin(), Triplets_M4.end());
    M4 =M4 *D_edge;

    SolutionVectorType  Sec_m_vol = SolutionVectorType::Zero(n_vol);
    SolutionVectorType  Sec_m_edge =  SolutionVectorType::Zero(n_edge);

    comp_sec_menb( Th.get(),  H_b ,  sol.f, sol.isNeumann,  sol.g_n, A ,  B , Conv_P ,
    Sec_m_vol ,  Sec_m_edge );

    //resize matrix for Dirichlet Boundary conditions
    SparseMatrixType Resize_Dir(nunkw-n_edge_Dir,nunkw);
    int counter_Fb_Dir = 0;
    for(Integer iF = 0; iF < Th->numberOfFaces(); iF++){
        if(!(sol.isDirichlet(Th->face(iF)))){
            Resize_Dir.insert(iF - counter_Fb_Dir, iF)=1.;
        }//if isBoundary
    else{counter_Fb_Dir++;};
    }//for iF

    SparseMatrixType GLOBAL_LHS = Resize_Dir * (M4 -M3*invM1 * M2) * (Resize_Dir.transpose());
    Eigen::VectorXd GLOBAL_RHS = Resize_Dir * (Sec_m_edge - M3*invM1 * Sec_m_vol);


    c_assembly.stop();
    cout << "Matrix assembly ends" << endl;
    std::cout << FORMAT(50) << "time_assembly"  << c_assembly.diff() << std::endl;

    // Solve linear system and reconstruct mesh unknowns

    common::chrono c_solve;
    c_solve.start();

    cout << "Linear solving begins" << endl;

    SolutionVectorType X = SolutionVectorType::Zero(nunkw);
    SolutionVectorType U = SolutionVectorType::Zero(n_vol_unkw);

    Eigen::SparseLU<SparseMatrixType> solver_infty;
    // Compute the ordering permutation vector from the structural pattern of ...
    solver_infty.analyzePattern(GLOBAL_LHS);
    std::cout << FORMAT(50) << "analyse pattern OK"  << std::endl;
    // Compute the numerical factorization
    solver_infty.factorize(GLOBAL_LHS);
    std::cout << FORMAT(50) << "factorisation etat stationnaire  OK"  << std::endl;
    //Use the factors to solve the linear system
    X = (Resize_Dir.transpose())*(solver_infty.solve(GLOBAL_RHS));
    U = invM1 * (Sec_m_vol - M2*X);
    X = X +X_b;

    c_solve.stop();
    std::cout << FORMAT(50) << "fin factorisation et calcul des inconnues "  << std::endl;
    std::cout << FORMAT(50) << "time_solve"  << c_solve.diff() << std::endl;
    Real Condi;

    if(Spec_info){
        common::chrono c_spectral;
        c_spectral.start();
        std::cout << FORMAT(50) << "Computation of spectral info begins"  << std::endl;

        //SparseGenMatProd<Real> op(GLOBAL_LHS);

        //GenEigsSolver< Real, LARGEST_MAGN, SparseGenMatProd<Real> > eigs(&op, 10, 30);

        SparseMatrixType Sing_val_mat = ( GLOBAL_LHS.transpose() )* GLOBAL_LHS;
        SparseSymMatProd<Real> op(Sing_val_mat);
        //SparseSymShiftSolve<Real> op(Sing_val_mat);
        //SparseSymMatProd <Real> op( ( GLOBAL_LHS.transpose() )* GLOBAL_LHS ) ;
        SymEigsSolver< Real, BOTH_ENDS, SparseSymMatProd <Real> > eigs(&op, 2, n_edge - n_edge_Dir -10);

        // Initialize and compute
        eigs.init();
        int nconv = eigs.compute();

        // Retrieve results
        Eigen::VectorXcd evalues;
        if(eigs.info() == SUCCESSFUL)
            evalues = eigs.eigenvalues();

            Condi = sqrt(abs(evalues(0)/evalues(1)));

        std::cout << "Eigenvalues found:\n" << evalues << std::endl;


        c_spectral.stop();
        std::cout << FORMAT(50) << "Computation of spectral info ends"  << std::endl;
        std::cout << FORMAT(50) << "time_spectral_quantities"  << c_spectral.diff() << std::endl;
    }
    else{ Condi = 0. ; }


    std::cout << sep << std::endl;
    std::cout << FORMAT(50) << "Errors"  << std::endl;
    std::cout << sep << std::endl;
    //------------------------------
    // Compute errors


    Real err_uh = 0.;
    Real err_en = 0.;
    Real norm_u_l2 = 0.;
    Real norm_u_h1 = 0.;


    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        const Point & x_T = T.center();
        const int & m = T.numberOfFaces();

        //Compute L2-norm of the exact solution
        norm_u_l2 += m_T*std::pow( sol.u(x_T), 2);
        // Compute local errors in L2-norm
        err_uh += m_T*std::pow( U(iT) - sol.u(x_T), 2);

        // Compute local error in energy-like norm (discrete H^1-norm)
          for(int iF_loc=0;iF_loc<m;iF_loc++ ){
          const int & iF=T.faceId(iF_loc);
          const Face & F = Th->face(iF);
          const Real & m_F = F.measure();
          const Point&  x_F = F.barycenter();
          const Point & n_F= F.normal(x_T);
          const Real & d_F = inner_prod(n_F, x_F-x_T);

          err_en += (m_F/d_F)*std::pow(U(iT)-X(iF)-sol.u(x_T)+sol.u(x_F),2);
          norm_u_h1 += (m_F/d_F)*std::pow(sol.u(x_T)-sol.u(x_F),2);

        }// for iF_loc
      } // for iT

    norm_u_l2 = std::sqrt(norm_u_l2);
    norm_u_h1 = std::sqrt(norm_u_h1);
    err_uh = std::sqrt(err_uh);
    err_en = std::sqrt(err_en);
    std::cout << FORMAT(50) << "les erreurs affichées sont des erreurs relatives"  << std::endl;

    std::cout << FORMAT(50) << "err_uh_rel - L2 " << err_uh / norm_u_l2 << std::endl;
    std::cout << FORMAT(50) << "err_uh_rel - H1" << err_en / norm_u_h1 << std::endl;
    std::cout << FORMAT(50) << "Minimum on mesh" << U.minCoeff() << std::endl;
    std::cout << FORMAT(50) << "Minimum on edge" << X.minCoeff() << std::endl;
    std::cout << FORMAT(50) << "Condition number" << Condi << std::endl;


    // export file vtu
    if(Visu){
        std::vector<Eigen::VectorXd> coeffs_u(Th->numberOfCells());
        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
        Eigen::VectorXd uh_F (1);
        uh_F(0) =U(iT);
        coeffs_u[iT] = uh_F;
        }//for iT
        string name_file = "output/uh_lin_eps="+to_string(epsilon);
        name_file += Mesh_file.substr(10);
        name_file += ".vtu";
        char* char_name;
        char_name = &name_file[0];

        postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>(char_name, Th.get(), coeffs_u);
    } //if Visuvector<Real> Vec_data;

    vector<Real> Vec_data;
    Vec_data.push_back(h);
    Vec_data.push_back(err_uh / norm_u_l2 );
    Vec_data.push_back(err_en / norm_u_h1 );
    Vec_data.push_back(U.minCoeff());
    Vec_data.push_back(X.minCoeff());
    Vec_data.push_back(Condi);

    std::cout << sep << std::endl;
    std::cout << FORMAT(50) << "DONE"  << std::endl;
    std::cout << sep << std::endl;

    return Vec_data;
}

vector<Real> nonlinearsta_mesh(const string & Mesh_file , const bool Visu, Integer argc, char * argv[]){

  std::string sep = "\n----------------------------------------\n";
  Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
  std::cout << "test1 ? "  << std::endl;

  // Read options and data
  GetPot options(argc, argv);

  std::string mesh_file = Mesh_file;


  // Pretty-print floating point numbers
  std::cout.precision(2);
  std::cout.flags(std::ios_base::scientific);

  std::cout << "test2 ? "  << std::endl;
  // Create mesh
  std::shared_ptr<Mesh> Th(new Mesh());
  if(mesh_file.find("typ1")!=std::string::npos)
    Th->readFVCA5File(mesh_file.c_str());
  else if(mesh_file.find("dgm")!=std::string::npos)
    Th->readDGMFile(mesh_file.c_str());
  else {
    std::cerr << "Unknown mesh format" << std::endl;
    exit(1);
  }

  std::cout << "test3 ? "  << std::endl;

  Th->buildFaceGroups();
  std::cout << "MESH: " << mesh_file << std::endl;
  std::cout << FORMAT(50) << "num_cells" << Th->numberOfCells() << std::endl;
  std::cout << FORMAT(50) << "num_faces" << Th->numberOfFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws_cells" << Th->numberOfCells()  << std::endl;
  std::cout << FORMAT(50) << "num_unknws_faces" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws" << Th->numberOfCells() +  Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
  std::cout << FORMAT(50) << "num_unknws_with_elimination" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;


  //------------------------------------------------------------------------------
  // Estimate mesh size
  std::cout<< FORMAT(50) << "Paramètre de stabilisation" << Eta << std::endl;

  Real h = Th->meshsize();
  std::cout << FORMAT(50) << "meshsize" << h << std::endl;

  //------------------------------------------------------------------------------
  // Exact solution

  ho::pho::Regular sol;

  //------------------------------------------------------------------------------
  // Assemble matrix
  //------------------------------------------------------------------------------


  int nunkw = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
  int n_vol_unkw =  Th->numberOfCells();

  // Pre computation
  std::cout << "Pre-computation begins" << endl;
  common::chrono c_precomput;
  c_precomput.start();

  std::vector<vector<vector<Real>>> A;
  comp_A_elt(Th.get(), sol.Lambda, Eta, A);

  std::vector<vector<Real>> B;
  comp_B_elt(Th.get(), A, B );

  std::vector<Real> Alpha;
  comp_Alpha_elt(Th.get(), B, Alpha );

  std::vector<vector<LocalVectorType>> Flux;
  precomp_flux(Th.get(), A, B, Flux );

  std::vector<LocalVectorType>  Sum_Flux;
  precomp_Somme_flux(Th.get(), B, Sum_Flux);

  std::vector<LocalVectorType>  Reconstruction;
  precomp_reconstruc(Th.get(),Reconstruction);

  //Discrete potential
  SolutionVectorType Phi_edge = SolutionVectorType::Zero(nunkw);
  SolutionVectorType Phi_vol = SolutionVectorType::Zero(n_vol_unkw);

    for(int iT=0; iT< n_vol_unkw; iT ++){
        const Cell & T = Th->cell(iT);
        const Point & x_T = T.center();
        Phi_vol(iT) = sol.Phi(x_T);
        } // for iT

    for(int iF=0 ; iF< nunkw ; iF++){
      const Face & F = Th->face(iF);
      const Point & x_F = F.barycenter();
      Phi_edge(iF) = sol.Phi(x_F);
  }//for iF

  std::vector<vector<Real>>  Flux_Phi;
  comp_flux_potential(Th.get(), Flux, Phi_vol , Phi_edge , Flux_Phi);

  std::vector<Real>  Sum_Flux_Phi;
  comp_Sumflux_potential(Th.get(), Sum_Flux, Phi_vol,  Phi_edge, Sum_Flux_Phi);

  std::vector<LocalMatrixType>  Mat_loc;
  comp_Mat_loc_bilin(Th.get(), Flux,  Sum_Flux, Mat_loc);
  std::cout << FORMAT(50) << "Matrice locale "  << std::endl;
  std::cout  << Mat_loc[0]  << std::endl;


  c_precomput.stop();
  cout << "Pre-computation ends" << endl;
  std::cout << FORMAT(50) << "time_precomputation"  << c_precomput.diff() << std::endl;

  //Creation condition initiale discrete
   std::cout << "Discretisation of loading term and equilibrium" << endl;

  Real Masse = 0.;

  SolutionVectorType F = SolutionVectorType::Zero(n_vol_unkw);
  SolutionVectorType G_Neu = SolutionVectorType::Zero(nunkw);
  SolutionVectorType X_b = SolutionVectorType::Zero(nunkw);
  SolutionVectorType U_eq = SolutionVectorType::Zero(n_vol_unkw);
  SolutionVectorType X_eq = SolutionVectorType::Zero(nunkw);
  SolutionVectorType X_b_eq = SolutionVectorType::Zero(nunkw);


  for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
    const Cell & T = Th->cell(iT);
    const Real & m_T = T.measure();
    const Point & x_T = T.center();
    F(iT) = m_T * sol.f(x_T);
    U_eq(iT) = exp(-sol.Phi(x_T));
    }//for iT

  std::cout << FORMAT(50) << "Discrete mass"  << Masse << std::endl;

  for(int iF = 0; iF< nunkw;iF++){
      const Face & F = Th->face(iF);
      const Point & x_F = F.barycenter();
      X_eq(iF) = exp(-sol.Phi(x_F));
      if(F.isBoundary()){
        X_b(iF) = sol.u_b(x_F);
        X_b_eq(iF) = exp(-sol.Phi(x_F));
        G_Neu(iF) = (F.measure()) * sol.g_n(x_F);
       } //if F is boundary
      }
//
//  std::vector<SolutionVectorType> Sol_faces;
//  std::vector<SolutionVectorType> Sol_maille;
//  std::vector<Real> Min_on_mesh;
//  std::vector<Real> Min_on_edge;
//  //Sol_maille.reserve(2*Nb_iter);
//  std::vector<Real> Temps;
//  std::vector<int> Nb_iter_Newton;
//
//  Sol_maille.push_back(U_0);
//  Sol_faces.push_back(X_0);
//  Temps.push_back(0.);
//  Min_on_mesh.push_back(U_0.minCoeff());
//  Min_on_edge.push_back(X_0.minCoeff());
//  Nb_iter_Newton.push_back(0.);

  std::cout << "Discretisation ends" << endl;

  SolutionVectorType U = SolutionVectorType::Zero(n_vol_unkw);
  SolutionVectorType X = SolutionVectorType::Zero(nunkw);


  U = U_eq ;
  X = X_eq ;
  Real s =0.;
  Real pas = 1.;
  int n_iter = 0;
  vector<Real> List_s;
  List_s.push_back(s);

  vector<int> Nb_iter_Newton;
  bool iter_is_pos;
  bool res_are_small;
  bool too_much_iter;
  bool Newton_is_CV;
  bool Newton_can_continue;

  // std::cout << X_eq << endl;
  //cin >> s;

  while( List_s.back() < 1.- std::pow(10.,-14)){

      n_iter += 1;

      do{ //boucle newton temps adaptatif
          std::cout << FORMAT(50) << "n_iter "<< n_iter << std::endl;
          SolutionVectorType R_vol = SolutionVectorType::Zero(n_vol_unkw);
          SolutionVectorType R_edge = SolutionVectorType::Zero(nunkw);
          //std::cout << FORMAT(50) << "Sol_maille"<< Sol_maille[n_iter]<< std::endl;

          SolutionVectorType U_ini = proj_UKN_vol(Th.get(), s_pos,  U);
          SolutionVectorType X_ini = proj_UKN_edge(Th.get(), s_pos,  X);

          s = min(1.,List_s.back() + pas);
          cout << "s" << s<< endl;
          pas = s - List_s.back();

          int k = 0;
          std::cout << FORMAT(50) << "Newton go ! pas " << pas << endl;
	  //std::cout << X_eq << endl;
          do{ //boucle Newton

            Real Norm_G = comp_sol_iter_newton(Th.get(), s * F, sol.isDirichlet, s* X_b + (1-s) * X_b_eq, sol.isNeumann , s * G_Neu ,
                A, B, Alpha, Reconstruction , Flux , Flux_Phi, Sum_Flux, Sum_Flux_Phi,
                U_ini , X_ini, R_vol ,  R_edge );


            Real Norm_R_infty = std::max(R_vol.lpNorm<Eigen::Infinity>() , R_edge.lpNorm<Eigen::Infinity>());

            U_ini = R_vol + U_ini;
            X_ini = R_edge + X_ini;

            Real Norm_R_rel = (R_vol.lpNorm<1>() + R_edge.lpNorm<1>() ) /(U_ini.lpNorm<1>() + X_ini.lpNorm<1>());
            Real min_iter = min(U_ini.minCoeff() , X_ini.minCoeff());

            iter_is_pos = (min_iter > 0.);
            res_are_small =Norm_R_rel < s_R_rel; //Norm_R_infty < s_R && Norm_G < s_G &&
            k += 1;
            too_much_iter=(k>s_iter);

            Newton_is_CV = (!too_much_iter) && res_are_small && iter_is_pos;
            Newton_can_continue = iter_is_pos && (! res_are_small) && (! too_much_iter);



          std::cout << "iter Newton OK" << endl;
          //std::cout << FORMAT(50) << "Sol_maille"  << U_ini<< std::endl;
          std::cout << FORMAT(50) << "N_G"  << Norm_G<< std::endl;
          //std::cout << FORMAT(50) << "N_G _inf"  << << std::endl;
          std::cout << FORMAT(50) << "N_R rel"  <<  Norm_R_rel << std::endl;
          std::cout << FORMAT(50) << "N_R_infty"  <<  Norm_R_infty << std::endl;

          std::cout << FORMAT(50) << "Min_iteration"  <<  min_iter << std::endl;
          std::cout << FORMAT(50) << "Iter"  <<  k  << "\n" << std::endl;
          //std::cout << FORMAT(50) << "U "  <<  U_ini  << "\n" << std::endl;



            //}while( (k < s_iter) && (Norm_G > s_G || Norm_R > s_R )) ; //boucle Newton
          }while(Newton_can_continue) ; //boucle Newton

          std::cout << FORMAT(50) << "Sortie iteration Newton \n" << std::endl;
          //Newton_CV = (k == s_iter);
          std::cout << FORMAT(50) << "Valeur Newton_CV" << Newton_is_CV << std::endl;

          if(Newton_is_CV){

            std::cout << FORMAT(50) << "Calcul pour s =" << s << "OK"<< std::endl;

            Nb_iter_Newton.push_back(k);
            List_s.push_back(s);
            U = U_ini;
            X = X_ini;

            pas = pas * (1. + 0.8);
          } //if Newton CV

          else{ pas = pas / 2.;}

        }while(!Newton_is_CV); // boucle newton avec pas temps adaptatif

        std::cout << "changement s " << std::endl;

  } //boucle temps


    std::cout << sep << std::endl;
    std::cout << FORMAT(50) << "Errors"  << std::endl;
    std::cout << sep << std::endl;
    //------------------------------
    // Compute errors


    Real err_uh = 0.;
    Real err_en = 0.;
    Real norm_u_l2 = 0.;
    Real norm_u_h1 = 0.;


    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        const Point & x_T = T.center();
        const int & m = T.numberOfFaces();

        //Compute L2-norm of the exact solution
        norm_u_l2 += m_T*std::pow( sol.u(x_T), 2);
        // Compute local errors in L2-norm
        err_uh += m_T*std::pow( U(iT) - sol.u(x_T), 2);

        // Compute local error in energy-like norm (discrete H^1-norm)
          for(int iF_loc=0;iF_loc<m;iF_loc++ ){
          const int & iF=T.faceId(iF_loc);
          const Face & F = Th->face(iF);
          const Real & m_F = F.measure();
          const Point&  x_F = F.barycenter();
          const Point & n_F= F.normal(x_T);
          const Real & d_F = inner_prod(n_F, x_F-x_T);

          err_en += (m_F/d_F)*std::pow(U(iT)-X(iF)-sol.u(x_T)+sol.u(x_F),2);
          norm_u_h1 += (m_F/d_F)*std::pow(sol.u(x_T)-sol.u(x_F),2);

        }// for iF_loc
      } // for iT

    norm_u_l2 = std::sqrt(norm_u_l2);
    norm_u_h1 = std::sqrt(norm_u_h1);
    err_uh = std::sqrt(err_uh);
    err_en = std::sqrt(err_en);
    std::cout << FORMAT(50) << "les erreurs affichées sont des erreurs relatives"  << std::endl;

    std::cout << FORMAT(50) << "err_uh_rel - L2 " << err_uh / norm_u_l2 << std::endl;
    std::cout << FORMAT(50) << "err_uh_rel - H1" << err_en / norm_u_h1 << std::endl;
    std::cout << FORMAT(50) << "Minimum on mesh" << U.minCoeff() << std::endl;
    std::cout << FORMAT(50) << "Minimum on edge" << X.minCoeff() << std::endl;
    cout << FORMAT(50) << "Number of iteration (change of s)" << n_iter << endl;

    // export file vtu
    if(Visu){
        std::vector<Eigen::VectorXd> coeffs_u(Th->numberOfCells());
        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
        Eigen::VectorXd uh_F (1);
        uh_F(0) =U(iT);
        coeffs_u[iT] = uh_F;
        }//for iT
        string name_file = "output/uh_nonlin";
        name_file += Mesh_file.substr(10);
        name_file += ".vtu";
        char* char_name;
        char_name = &name_file[0];

        postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>(char_name, Th.get(), coeffs_u);
    } //if Visuvector<Real> Vec_data;

    vector<Real> Vec_data;
    Vec_data.push_back(h);
    Vec_data.push_back(err_uh / norm_u_l2 );
    Vec_data.push_back(err_en / norm_u_h1 );
    Vec_data.push_back(U.minCoeff());
    Vec_data.push_back(X.minCoeff());
    Vec_data.push_back(0.);

    std::cout << sep << std::endl;
    std::cout << FORMAT(50) << "DONE"  << std::endl;
    std::cout << sep << std::endl;

    return Vec_data;

}



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

