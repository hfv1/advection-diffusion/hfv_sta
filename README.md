# hfv_sta

## README

### Description

The code hfv_sat is an implementation of different Hybrid Finite Volume schemes for linear stationary advection-diffusion equations.

This code was developped in order to investigate the interest of using nonlinear schemes to discretise dissipative problems on general meshes.
It computes a numerical approximation of a transcient anisotropic advection-diffusion equation.
Three schemes are implemented:
* the classical (linear) HMM scheme introduced in [this paper](https://hal.science/hal-00808695);
* a linear scheme based on the [exponential fitting strategy](https://epubs.siam.org/doi/10.1137/0726078);
* a nonlinear scheme which follows the ideas of the VAG scheme introduced in [this paper](https://hal.science/hal-01119735).

The nonlinear discretisation ensures that the computed solution is positive.

This code was used to get some numerical results presented in an [article](https://hal.science/hal-03281500) about accuracy of HFV schemes. <br>


#### Installation
 
The following softwares/libraries are required for compilation:

* C++
* CMake
* Eigen
* Boost 
* Spectra (does not need instalation, header-only C++ library, is already in the folder hfv)

To compile the code, you should create a build directory.


mkdir build                 <br>
cd build                    <br>
cmake ..                    <br>
make                        

The compilation will create a hfv directory, in which you can find and execute "hfv_advdiff_evol".
In order to get some data about visualisation, you should create an "output" directory in the build directory.

mkdir output               <br>
./bin/hfv_advdiff_sta     <br>


### Getting started


The data of the problem (source term, boundary values, physical data) can be specified in the ExactSolution constructor (file data.h).
Some specific test case are already available in this file, and are commented.<br>
The execution of hfv_advdiff_hfv will perform a small benchmark between schemes.
You can either select a specific mesh family or launch the benchmark on all the meshes available (note that this can take some time).
Some folder "build/mesh_name" will then be created, and file describing the errors will be generated inside.

The visualisation files (.vtu format) can be read directcly using Paraview, and can be obtained by modifying the boolean "Visu" in the function "bench".
Similarly, one can obtained information abbout the condition number of the matrices by modifying the value of "Spectral_info" (note that the computation of these numbers is quite long).

Note that the "omega scheme" (function "omegaschema" of the code) correspond to a scheme which encompasses the HMM and the exponential fitting scheme. 
The first argument should be equal to 0 for the HMM method, and 1 for the exponential fitting.  

### Authors 

Author: Julien Moatti (julien.moatti@tuwien.ac.at)

### License

GNU General Public License v3.0

### Project status

This has been created for personnal use. There might or might not be updates in the future.