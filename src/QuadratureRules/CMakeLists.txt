ADD_LIBRARY(QuadratureRules
  DunavantQuadratureRule.cpp
  FaceIntegrator.cpp
  PyramidIntegrator.cpp
  triangle_dunavant_rule.cpp
)