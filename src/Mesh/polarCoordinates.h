#ifndef POLARCOORDINATES_H
#define POLARCOORDINATES_H

#include "Common/defs.h"

Point polarCoordinates(const Point & a_P);

#endif
