// -*- C++ -*-
#ifndef TIKZWRITER_H
#define TIKZWRITER_H

#include "Mesh/Mesh.h"

void TikZWriter(const Mesh & Th, const std::string & file_name, bool plot_centers = false);

#endif
