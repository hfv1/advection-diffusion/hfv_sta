#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "Face.h"
#include "vectorProduct.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
template<>
void Face::_compute_measure<2>()
{
  m_measure = norm_2(m_points[1] - m_points[0]);
}

/*----------------------------------------------------------------------------*/

template<>
void Face::_compute_measure<3>()
{
  Point u, v, n;
  m_measure = 0.;
  for(Integer i=1; i<m_number_of_points-1; i++) {
    u = m_points[i] - m_points[0];
    v = m_points[i+1] - m_points[0];
    n = vector_product<DIM>(u, v);
    m_measure += norm_2(n)/2.;
  }
}

/*----------------------------------------------------------------------------*/

template<>
void Face::_compute_normal<2>()
{
  Point u;
  u = m_points[1] - m_points[0];
  m_normal(0) = -u(1);
  m_normal(1) = u(0);
  m_normal /= norm_2(m_normal);
}

/*----------------------------------------------------------------------------*/

template<>
void Face::_compute_normal<3>()
{
  Point u, v;
  u = m_points[1] - m_points[0];
  v = m_points[2] - m_points[0];
  m_normal = vector_product<DIM>(u, v);
  m_normal /= norm_2(m_normal);
}

/*----------------------------------------------------------------------------*/

template<>
void Face::_compute_center_of_gravity<2>()
{
  _compute_barycenter();
  m_properties |= P_BARYCENTER;
  m_center_of_gravity = m_barycenter;
}

/*----------------------------------------------------------------------------*/

template<>
void Face::_compute_center_of_gravity<3>()
{
  _compute_measure<3>();
  m_properties |= P_MEASURE;

  m_center_of_gravity = ZeroRealVector(DIM);
  for(Integer i=1; i<m_number_of_points-1; i++) {
    Point u = m_points[i] - m_points[0];
    Point v = m_points[i+1] - m_points[0];
    // Center of gravity of the triangle 0,i,i+1
    m_center_of_gravity += (m_points[0]+m_points[i]+m_points[i+1])*norm_2(vector_product<DIM>(u, v))/3.;
  }
  
  m_center_of_gravity /= 2.*m_measure;
  // _compute_barycenter();
  // m_properties |= P_BARYCENTER;
  // m_center_of_gravity = m_barycenter;

}

/*----------------------------------------------------------------------------*/

Integer Face::backCell() const
{
  return m_back_cell;
}

/*----------------------------------------------------------------------------*/

Integer Face::frontCell() const
{
  return m_front_cell;
}

/*----------------------------------------------------------------------------*/

Integer & Face::backCell()
{
  return m_back_cell;
}

/*----------------------------------------------------------------------------*/

Integer & Face::frontCell()
{
  return m_front_cell;
}

/*----------------------------------------------------------------------------*/

//! Compute selected properties
void Face::compute(Integer a_properties)
{
  m_properties = a_properties;

  if(m_number_of_points == 2) { // 2d
    if(m_properties & P_NORMAL)
      _compute_normal<2>();
    if(m_properties & P_BARYCENTER && !(m_properties & P_CENTEROFGRAVITY))
      _compute_barycenter();
    if(m_properties & P_CENTEROFGRAVITY)
      _compute_center_of_gravity<2>();
    if(m_properties & P_MEASURE)
      _compute_measure<2>();  
  } else { // 3d
    if(m_properties & P_NORMAL)
      _compute_normal<3>();
    if(m_properties & P_BARYCENTER)
      _compute_barycenter();
    if(m_properties & P_CENTEROFGRAVITY)
      _compute_center_of_gravity<3>();
    if(m_properties & P_MEASURE && !(m_properties & P_CENTEROFGRAVITY))
      _compute_measure<3>();  
  }
}

/*----------------------------------------------------------------------------*/

void Face::addRegion(Integer a_region)
{
  m_physical_region = a_region;
}

/*----------------------------------------------------------------------------*/

Integer Face::getRegion() const
{
  return m_physical_region;
}

/*----------------------------------------------------------------------------*/

void Face::addPoint(Integer a_iP, const Point & a_P)
{
  VF_ASSERT(m_points.size() < m_number_of_points);
  m_points.push_back(a_P);
  m_point_ids.push_back(a_iP);
  m_properties = 0;
}

/*----------------------------------------------------------------------------*/

std::pair<Point, Integer> Face::point(Integer a_iP) const
{
  return std::pair<Point, Integer>(m_points[a_iP], m_point_ids[a_iP]);
}

/*----------------------------------------------------------------------------*/

const Point & Face::normal() const
{
  VF_ASSERT(m_properties & P_NORMAL);
  return m_normal;
}

/*----------------------------------------------------------------------------*/

Point Face::normal(const Point & a_P) const
{
  VF_ASSERT(m_properties & P_NORMAL);
  if(inner_prod(m_points[0]-a_P, m_normal)<0)
    return -m_normal;
  else
    return m_normal;
}

/*----------------------------------------------------------------------------*/

const Point & Face::barycenter() const
{
  VF_ASSERT(m_properties & P_BARYCENTER);
  return m_barycenter;
}

/*----------------------------------------------------------------------------*/

const Point & Face::centerOfGravity() const
{
  VF_ASSERT(m_properties & P_CENTEROFGRAVITY);
  return m_center_of_gravity;
}

/*----------------------------------------------------------------------------*/

const Real & Face::measure() const 
{
  VF_ASSERT(m_properties & P_MEASURE);
  return m_measure;
}

/*----------------------------------------------------------------------------*/

void Face::_compute_barycenter()
{
  m_barycenter = m_points[0];
  for(Integer i=1; i<m_number_of_points; i++)
    m_barycenter += m_points[i];
  m_barycenter /= (Real)m_number_of_points;
}

/*----------------------------------------------------------------------------*/

std::ostream & operator<<(std::ostream & a_ostr, const Face & a_F)
{
  a_ostr << "-- Face --" << std::endl;
  a_ostr << a_F.m_number_of_points << " nodes" << std::endl;
  a_ostr << "nodes             : " << std::flush;
  for(Face::IdVectorType::const_iterator iP=a_F.m_point_ids.begin(); iP!=a_F.m_point_ids.end(); iP++)
    a_ostr << *iP << " " << std::flush;
  a_ostr << std::endl;
  a_ostr << "back cell         : " << a_F.backCell() << std::endl;
  a_ostr << "front cell        : " << a_F.frontCell() << std::endl;
  a_ostr << "points            : " << std::endl;
  for(Face::PointVectorType::const_iterator iP=a_F.m_points.begin(); iP!=a_F.m_points.end(); iP++)
    a_ostr << *iP << " " << std::flush;
  a_ostr << std::endl;
  if(a_F.m_properties & Face::P_NORMAL)
    a_ostr << "normal            : " << a_F.normal() << std::endl;
  if(a_F.m_properties & Face::P_BARYCENTER)
    a_ostr << "barycenter        : " << a_F.barycenter() << std::endl;
  if(a_F.m_properties & Face::P_CENTEROFGRAVITY)
    a_ostr << "center of gravity : " << a_F.centerOfGravity() << std::endl;
  if(a_F.m_properties & Face::P_MEASURE)
    a_ostr << "measure           : " << a_F.measure() << std::flush;
  return a_ostr;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
